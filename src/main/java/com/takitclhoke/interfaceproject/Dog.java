/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.interfaceproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class Dog extends LandAnimal {
    
    private String species;

    public Dog(String species) {
        super("Dog", 4);
        this.species  = species;
    }

    @Override
    public void eat() {
        System.out.println("Dog: " + species + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Dog: " + species + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog: " + species + " sleep");
    }

    @Override
    public void run() {
        System.out.println("Dog: " + species + " run");
    }
    
}
