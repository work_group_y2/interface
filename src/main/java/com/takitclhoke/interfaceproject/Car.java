/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.interfaceproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class Car extends Vahicle implements Runable {

    public Car(String englne) {
        super(englne);
    }

    @Override
    public void startEnglne() {
        System.out.println("Car: startEnglne");
    }

    @Override
    public void stopEnglne() {
        System.out.println("Car: stopEnglne");
    }

    @Override
    public void ralseEnglne() {
        System.out.println("Car: ralseEnglne");
    }

    @Override
    public void applyEnglne() {
        System.out.println("Car: applyEnglne");
    }

    @Override
    public void run() {
        System.out.println("Car: run");
    }

}
