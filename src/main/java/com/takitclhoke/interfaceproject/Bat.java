/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.interfaceproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class Bat extends Poultry    {
    
    private String species;

    public Bat(String species) {
        super("Bat", 2);
        this.species = species;
    }

    @Override
    public void eat() {
         System.out.println("Bat: " + species + " eat");
    }

    @Override
    public void speak() {
       System.out.println("Bat: " + species + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + species + " sleep");
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + species + " fly");
    }
    
}
