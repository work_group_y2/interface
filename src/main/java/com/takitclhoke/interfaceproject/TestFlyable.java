/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.interfaceproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat("Batman");
        bat.eat(); //Animl,Poulty,Flyable
        bat.fly(); //Animl,Poulty,Flyable

        Plane plane = new Plane("Engine number I");
        plane.fly(); //Vahicle,Flyable

        Dog dog = new Dog("Rottweiler");

        Human human = new Human("Dang");

        Cat cat = new Cat("Sphynx");

        Puppy puppy = new Puppy("Parrot");

        Car car = new Car("Engine number IV");

        Flyable[] flyables = {bat, plane, puppy};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEnglne();
                p.run();
            }
            f.fly();
        }

        Runable[] runables = {dog, human, cat, car};
        for (Runable r : runables) {
            r.run();
        }
    }

}
