/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.interfaceproject;

/**
 *
 * @author ทักช์ติโชค
 */
public class Plane extends Vahicle implements Flyable, Runable {

    public Plane(String englne) {
        super(englne);
    }

    @Override
    public void startEnglne() {
        System.out.println("Plane: startEnglne");
    }

    @Override
    public void stopEnglne() {
        System.out.println("Plane: stopEnglne");
    }

    @Override
    public void ralseEnglne() {
        System.out.println("Plane: ralseEnglne");
    }

    @Override
    public void applyEnglne() {
        System.out.println("Plane: applyEnglne");
    }

    @Override
    public void fly() {
        System.out.println("Plane: fly");
    }

    @Override
    public void run() {
        System.out.println("Plane: run");
    }

}
